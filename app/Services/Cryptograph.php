<?php  


namespace App\Services;
use coinmarketcap\api\CoinMarketCap;


class CryptoGraph
{

    public function getcryptohistory($currency, $param='null'){

        $Api_url =  "https://www.okcoin.com/api/v1/kline.do?";

        if($param == '1hour')
        {
            $time = '1hour';
            $duration = '1';
            $url = $Api_url."symbol=".strtolower($currency)."_usd&type=$time";
        }elseif ($param == '12hour') {
            $time = '12hour';
            $duration = '3';
            $url = $Api_url."symbol=".strtolower($currency)."_usd&type=$time";
        }elseif ($param == '1day') {
            $time = '1day';
             $duration = '9';
             $url = $Api_url."symbol=".strtolower($currency)."_usd&type=$time";
        }elseif ($param == '3day') {
            $time = '3day';
             $duration = '30';
             $url = $Api_url."symbol=".strtolower($currency)."_usd&type=$time";
        }else {
             $time = '1week';
             $duration = '1';
             $url = $Api_url."symbol=".strtolower($currency)."_usd&type=$time";
             
        }


           $data = file_get_contents($url);
        //    sort($data, SORT_NATURAL);
        //    $data1=sort($data);
        //    dd($data);
        
        $contents = [];
        $contents['Data'] = json_decode($data,true);

        // $dataHistory = $contents['Data'];

         return $contents;
     }

     public function get_crypto_currency_data($currency)
     {
     	return CoinMarketCap::getCurrencyTicker($currency,"usd");
     }

     public function get_homepage_graph($currency)
     {
        $url = "https://min-api.cryptocompare.com/data/histoday?fsym=$currency&tsym=USD&limit=10&aggregate=9&e=CCCAGG";

        $data = file_get_contents($url);

        $contents = json_decode($data,true);

        $graphdata = $contents['Data'];

         return $graphdata;
     }

}




?>