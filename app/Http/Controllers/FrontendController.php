<?php

namespace App\Http\Controllers;

use App\Repositories\GlobalCoinMarketCap;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    //
    private $totalCoinMarket;
    public function __construct(GlobalCoinMarketCap $totalCoinMarket){
        $this->totalCoinMarket = $totalCoinMarket;
    }
    public function index(){
        $data_news = $this->totalCoinMarket->get_overall_news();
        $marquee_data = $this->totalCoinMarket->get_overal_coinmarket_marquee();
        $data_to_display_in_page = $this->totalCoinMarket->get_overal_coinmarket_status();
        // dd($data_to_display_in_page);
        
         return view('frontpage', compact('data_to_display_in_page','data_news','marquee_data'));
    	
    }



      public function displaynews(){

        $data_news = $this->totalCoinMarket->get_overall_news();
        return view('frontpage', compact('data_to_display_in_page'));
    }

    public function displayHistory($symbol, $currency)
    {
        //$dataHistory =  $this->totalCoinMarket->get_currency_history($symbol);

        $dataHistory = [];

        $datacurrency = $this->totalCoinMarket->get_currency_ticker($currency);

        $historygraphData =[];

         foreach ($dataHistory as $history ) {

            $time = date('d M y', $history['time']);

              $historygraphData[] = [
                'label' => $time,
                'y' => $history['close'],
              ];
          }
      return view('cryptohistory', compact('historygraphData','dataHistory','datacurrency'));
    }

    public function historical($symbol, $currency)
    {
         $data_news = $this->totalCoinMarket->get_overall_news();
        $marquee_data = $this->totalCoinMarket->get_overal_coinmarket_marquee();
        $data_to_display_in_page = $this->totalCoinMarket->get_overal_coinmarket_status();
        return view('currencydetail', compact('data_to_display_in_page','data_news','marquee_data','symbol','currency'));

      // return view('currencydetail')->with(compact('symbol', 'currency'));
    }

    public function historicalgraph(Request $request)
    {
      if (request()->ajax()) {

       $currency =  $request->currency;
       $symbol = $request->symbol;
       $param = $request->param;
      
      $datacurrency = $this->totalCoinMarket->get_currency_ticker($currency);
        // dd($datacurrency);
      $dataHistory =  $this->totalCoinMarket->get_currency_history($symbol, $param);
      // $dataHistory= $this->totalCoinMarket->getcryptohistory($symbol, $param);
        // dd($dataHistory);
      $historygraphDatalabels =[];
      $historygraphDataset =[];
      $mydata = [];
         foreach ($dataHistory as $history ) {
           // dd($history);
            foreach($history as $key => $data){
                $mydata[$key] = [];
              $mydata[$key]['time'] =$data[0];
              $mydata[$key]['close']=$data[4];
              $mydata[$key]['high']=$data[2];
              $mydata[$key]['low']=$data[3];
              $mydata[$key]['vol']=$data[5];
            }
        
          // dd($history);
            // if($param == '1day')
            // {
            //   $time = date('d M y H:i', $history['time']);
            // }else
            // {
            //   $time = date('d M y', $history['time']);
            // }
            
            // $historygraphDataset[] = $history['close'];

            // $historygraphDatalabels[] = $time;
                           
          }
          // $price = array();
          $count=[];
          foreach ($mydata as $key => $row)
          {
              $count[$key] = $row['time'];
          }
          array_multisort($count, SORT_DESC, $mydata);
          // dd($mydata);

          $mydata = collect($mydata)->sortBy('time')->reverse()->toArray();
          
          // array_reverse($mydata, true);
          // dd($mydata);
          return response()->json(compact( 'datacurrency','dataHistory','historygraphDatalabels','historygraphDataset','mydata'));
       }
    }

    // get Graph data for homepage
    public function getGraphData(Request $request)
    {
      
      $symbol = $request->symbol;

      //$symbol = "BTC";
   
      $dataHistory =  $this->totalCoinMarket->get_homepage_graphdata_repo($symbol);

      $historygraphDatalabels =[];
      $historygraphDataset =[];

         foreach ($dataHistory as $history ) {
          $time = date('d M y', $history['time']);
          $historygraphDataset[] = $history['close'];
          $historygraphDatalabels[] = $time;
                           
       }

          return response()->json(compact('historygraphDatalabels','historygraphDataset'));
    }

}
