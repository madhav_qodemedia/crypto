<?php

namespace App\Http\Controllers;

use App\Repositories\CurrencyList;
use App\Repositories\GlobalCoinMarketCap;
use Illuminate\Http\Request;

class HomeController extends Controller
{

	private $currencylist;
    private $totalCoinMarket;
   
     public function __construct(CurrencyList $currencylist, GlobalCoinMarketCap $totalCoinMarket)
    {
        $this->currencylist = $currencylist;
        $this->totalCoinMarket = $totalCoinMarket;
     }


     //for loading more data load
    public function getCryptocurrencyData(Request $request)
    {

          $currencyfrom =  $request->count;

        $data_to_display_in_page = $this->totalCoinMarket->getCryptoCurrencyDataList($currencyfrom);

          return response()->json(compact( 'data_to_display_in_page'));
    }


    public function getCurrencyList()
    {

    	$this->currencylist->getCurrencyList();

    	//if (request()->ajax()) {

       // return response()->json();

    	//}
    }


    public function getExchangeValue(Request $request)
    {

         $currencyfrom =  $request->currencyfrom;
         $currencyto = $request->currencyto;

        $currencies = $this->currencylist->currencyexchange($currencyfrom,$currencyto);

        //dd($currencies);

        return response()->json(compact( 'currencies'));
    }

    public function getcryptocurrencysearch(Request $request)
    {
        $queryString =  $request->queryString;

        $currencies = ['btc','etc']; 

        return response()->json(compact( 'currencies'));
    }
}
