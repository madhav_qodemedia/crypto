<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 12/14/2017
 * Time: 9:36 PM
 */

namespace App\Repositories;

use App\Services\CryptoNews;
use App\Services\CryptoRates;
use App\Services\CryptoGraph;


class GlobalCoinMarketCap
{


    private $cryptoNews;
    private $cryptoRates;
    private $cryptograph;

     public function __construct(CryptoNews $cryptoNews, CryptoRates $cryptoRates, CryptoGraph $cryptograph)
    {
        $this->cryptoNews = $cryptoNews;
        $this->cryptoRates = $cryptoRates;
        $this->cryptograph = $cryptograph;
    }

    // get coin list
    public function get_overal_coinmarket_status(){
      return $this->cryptoRates->get_overal_coinmarket_status();
    }

    //get currency data list for load more
     public function getCryptoCurrencyDataList($currencyfrom){
      return $this->cryptoRates->getCryptoCurrencyDataForLoadMore( $currencyfrom);
    }

    //get news list
     public function get_overall_news(){
      return $this->cryptoNews->get_overall_news();
    }

    //get coinmarket marquee list
    public function get_overal_coinmarket_marquee(){

        $url='https://api.coinmarketcap.com/v1/ticker/?limit=50';
        // $url='https://www.okcoin.com/api/v1/trades.do?since=5000';
        $data = file_get_contents($url); // put the contents of the file into a variable

        $criptocurrenccy_list = json_decode($data, true);

        foreach($criptocurrenccy_list AS $key => $newArray) {
            $tempArray[$key] = $newArray['price_usd'];
        }

        natsort($tempArray);
        $finalArray = array();

        foreach($tempArray AS $key => $value) {
            $finalArray[] = $criptocurrenccy_list[$key];
        }

        return $finalArray;
    }


    //get cryptonews history
    public function get_currency_history($currency, $param){
        // dd('hisdjnfjd');
        // $test = $this->cryptograph->getcryptohistory($currency,$param);
        // dd($test);
      return $this->cryptograph->getcryptohistory($currency,$param);
    }




    public function get_all_coinmarket(){
        $all_currency='https://api.coinmarketcap.com/v1/ticker?limit=30000';
        $all_currency_data = file_get_contents($all_currency);
        $data_to_display  = json_decode($all_currency_data,True);
        return $data_to_display;
    }

    public function get_pagelist_coinmarket($page){
        if(isset($page)){
            $start_value= $page * 100;
        }else{
            $start_value= 0;
        }

        $currency_to_show='https://api.coinmarketcap.com/v1/ticker?start='.$start_value.'&limit=100';
        $all_page_list_currency_data = file_get_contents($currency_to_show);
        $data_to_display_in_page  = json_decode($all_page_list_currency_data,True);
        return $data_to_display_in_page;
    }


    public function get_search_coinmarket($search)
    {
        $all_currency = 'https://api.coinmarketcap.com/v1/ticker?start=1&limit=30000';
        $all_currency_data = file_get_contents($all_currency);
        $data_to_display = json_decode($all_currency_data, True);
        foreach ($data_to_display as $searchValue) {

            $searchItem = strtoupper($searchValue['symbol']);
            if ($searchItem == $search) {
                return $searchValue;
            }else{
                return 0;
            }

        }

        return $data_to_display;
    }

   public function get_pagelist_coinmarketNews($page){
        
        if(isset($page)){
            $start_value= $page * 10;
        }else{
            $start_value= 0;
        }

        $currency_to_show_news='https://newsapi.org/v2/everything?sources=crypto-coins-news&apiKey=d406153f450c429abe62568aa3bf29c4&start='.$start_value.'&limit=10';
        $all_page_list_currency_data = file_get_contents($currency_to_show_news);
        $contents = json_decode($all_page_list_currency_data,True);
        $cryptosNews = $contents['articles'];
       
        return array_slice($cryptosNews , $start_value);
    }

    public function get_currency_ticker($currency)
    {
         return $this->cryptograph->get_crypto_currency_data($currency);


    }

    //get Homepage graph Data
    public function get_homepage_graphdata_repo($currency)
    {
       return $this->cryptograph->get_homepage_graph($currency);
    }

}

