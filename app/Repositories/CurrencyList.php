<?php

namespace App\Repositories;
use App\Services\CoinList;
use App\Services\CurrencyExchange;


class CurrencyList
{

	private $currencyexchange;

     public function __construct(CurrencyExchange $currencyexchange)
    {
        $this->currencyexchange = $currencyexchange;
    }


	public function getCurrencyList()
	{
		$str = file_get_contents(url('/').'/currencies.json');
		$json = json_decode($str, true);

		dd($json);
	}


	public function currencyexchange($currencyfrom, $currencyto)
	{
		return $this->currencyexchange->getCurrencyAmount($currencyfrom, $currencyto);
	}

/*	public function getCurrencyList()
	{
		
	}*/

}


?>