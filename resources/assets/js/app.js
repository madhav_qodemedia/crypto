
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import moment from 'vue-moment';
Vue.use(moment);

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('articles-component', require('./components/articles.vue'));
Vue.component('navbar-component', require('./components/navbar.vue'));
Vue.component('crypto-exchange-component', require('./components/CryptoExchangeComponent.vue'));
Vue.component('maps-component', require('./components/MapsComponent.vue'));
Vue.component('infinite-scroll', require('./components/Infinite-scroll.vue'));
Vue.component('autocomplete', require('./components/Autocomplete.vue'));
Vue.component('purchase', require('./components/buy.vue'));


const app = new Vue({
    el: '#app'
});
 




