
<meta name="csrf-token" content="{{ csrf_token() }}">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.js"></script>
	<script src="../../js/vue-charts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.3/vue.min.js"></script>
<!-- 
    <link rel="stylesheet" href="v-infinite-scroll/dist/v-infinite-scroll.css"></link>
<script src="v-infinite-scroll/dist/v-infinite-scroll.js"></script> -->
    
<style type="text/css">
	
div#graph {
    max-width: 500px;
}

</style>
{{dd($symbol)}}
<div class="container">

<div id="app">


<!-- <infinite-scroll></infinite-scroll> -->

<crypto-exchange-component></crypto-exchange-component> 
<hr/>
	<navbar-component :symbol="'{{ $symbol }}'" :currency="'{{ $currency }}'" ></navbar-component>
<hr/>
<articles-component :symbol="'{{ $symbol }}'" :currency="'{{ $currency }}'"></articles-component>





</div><!--#app-->

</div><!--.container-->


	 	<div id="graph">
            <chartjs-line :labels="labels" :data="dataset" :bind="true"></chartjs-line>
        </div>


	<script src="/js/app.js"></script> 



    
