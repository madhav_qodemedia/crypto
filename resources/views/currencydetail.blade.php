<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>History for {{ $symbol }}</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.js"></script>
    <script src="../../js/vue-charts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.3/vue.min.js"></script>

    @include('layouts._styles')
    <style type="text/css">
        .price-tabination{
            border-radius: 17px;
            margin-top: -62px;
            width: 100%;
            background: #ffffff;
            box-shadow: 0px 0px 12px 0px rgba(0, 0, 0, 0.1);
        }


/* EXAMPLE 3 */
.input-search-3 {
    position: relative;
}

.input-search-3 input {
    height: 42px;
    padding-right: 40px;
}

.input-search-3 span {
    font-size: 20px;
    position: absolute;
    top: 8px;
    right: 8px;
    color: #ccc;
}
/* END EXAMPLE 3 */

    </style>
</head>
<body class="demo-2 loading imgloaded">
<div id="app">
        <div class="homepage_banner_below">
            <marquee behavior="scroll" direction="left" scrollamount="10" class="top_currency" >
            <div class="inner">
                <?php
                    foreach (array_reverse($marquee_data) as $marquee_arr) {
                    ?>[
                    <span class="sprite sprite-<?php  echo $marquee_arr['id'] ?> small_coin_logo" >  </span>
                    <span><?php echo $marquee_arr['id']; ?></span>
                    <span class="space_30">$<?php echo $marquee_arr['price_usd']; ?></span>
                    <span class="<?php echo ($marquee_arr['percent_change_24h'] > 0 ? 'positive_number' : 'negative_number');?>"><?php echo $marquee_arr['percent_change_24h']; ?></span>
                    ]<span class="space_30"></span>
                    <?php
                    }
                ?>
                </div>
            </marquee>
        </div><!-- .homepage_banner_below -->
        <!-- news marquee -->

        <div class="homepage_banner_letest_news">
            <div class="row no-gutters">
                <div class="col-sm-2">
                    <p class="latest_news">Latest News</p>
                </div>
                <div class="col-sm-10">
                    <div class="news-marquee">
                        <marquee behavior="scroll" direction="left" scrollamount="5" class="top_news">
                        <div class="inner">
                            <?php
                                foreach ($data_news as $news_arr) {
                            ?>
                                <span class="space_30"> | <a href="<?php echo $news_arr['url']; ?>" target="_blank"><?php echo $news_arr['title']; ?></a></span>
                            <?php
                            }
                            ?>
                            </div>
                        </marquee>
                    </div><!-- .news-marquee -->
                </div><!-- .col -->
            </div><!-- .row  -->
        </div><!-- .homepage_banner_letest_news  -->
      
        <articles-component :symbol="'{{ $symbol }}'" :currency="'{{ $currency }}'"></articles-component>
        <footer id="colophon" class="site-footer" role="contentinfo">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <ul class="footer-list">
                            <li><a href="#">©2018 Cryptolytics</a></li>
                            <li><a href="#">Trade Volume</a></li>
                            <li><a href="#">Trending</a></li>
                            <li><a href="#">Tools</a></li>
                        </ul>
                    </div><!-- .col -->
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="footer-calc">
                            <h4 class="footer-heading">Exchange Cryptocurrency at the best rate</h4>
                               <crypto-exchange-component></crypto-exchange-component>
                        </div>
                    </div><!-- .col -->
                </div><!-- .row -->
            </div><!-- .container -->
        </footer>

</div>

         <script type="text/javascript">
                    jQuery(window).scroll(function() {
                        if(jQuery(this).scrollTop()>1170) {
                            jQuery('.display-on-scroll').addClass("display-on-scroll-show");
                        }
                        else {
                            jQuery('.display-on-scroll').removeClass("display-on-scroll-show");
                        }
                    });
                </script>

                <script src="/js/app.js"></script> 
      <script src="https://code.highcharts.com/stock/highstock.js"></script>
      <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js"></script>
</body>
</html>

