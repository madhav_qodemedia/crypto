

<?php 

print_r($datacurrency );


foreach ($datacurrency as $currency) {
  echo "name : ".$currency['name']."<br/>";
  echo "price_usd : ".$currency['price_usd']."<br/>";
  echo "24h_volume_usd : ".$currency['24h_volume_usd']."<br/>";
  echo "market_cap_usd : ".$currency['market_cap_usd']."<br/>";
}


?>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<div id="chartContainer" style="width: 100%; height: 470px;display: inline-block;"></div>

<script type="text/javascript">
  


<?php 

//dd($historygraphData);

        $arr = array();
         foreach($historygraphData as $data){
           $arr[] = $data;
        } 


                    
?>

 var chart = new CanvasJS.Chart("chartContainer",{
    theme: "dark2",
   
    axisY: {
              
                lineColor: "#444444",
                tickColor: "#444444",
                gridColor: "#444444"
            },
            axisX: {
             
                lineColor: "#444444",
                valueFormatString: "DD-MMM" ,
                tickColor: "#444444"
            },
            legend:{
                verticalAlign: "top",
                fontSize: 16,
                dockInsidePlotArea: true
            },
            toolTip:{
               fontColor: "red",
               fontSize: 12,
               borderThickness: 2,
               cornerRadius: 5,
               backgroundColor: "#FFF",
               borderColor: "#CCC",
               Content: "{x} : {y}"
             },
    data: [{
    type: "line",
    lineColor:"#b2acdd",
        dataPoints : <?php echo json_encode($arr); ?>
    }]
});
 chart.render();


</script>




<table>
<tr>
<th>Date</th>
<th>Open</th>
<th>Low</th>
<th>High</th>
<th>Close</th>
</tr>
<?php

foreach($dataHistory as $data){
?>
          
  <tr>
      <td><?php echo date('d M y', $data['time']); ?></td>
      <td><?php echo $data['open']; ?></td>
      <td><?php echo $data['low']; ?></td>
      <td><?php echo $data['high']; ?></td>
      <td><?php echo $data['close']; ?></td>
  </tr> 

<?php

 } 

?>
</table>


