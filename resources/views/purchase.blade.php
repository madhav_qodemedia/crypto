<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type='text/css' href="https://cdn.jsdelivr.net/gh/runestro/525icons@5.5/fonts/525icons.min.css"> 


    </head>
    <body>
        
        <div id="buypage">
              
                        <div id="buypage">
                            <div class="content-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h2 class="page-title">Buy and Sell</h2>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="tables-section">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-lg-4 m-b-30">
                                                <div class="accordion">
                                                    <div class="card rounded-0">
                                                        <div class="card-header">
                                                            <h4 class="mb-0" data-toggle="collapse" data-target="#buy-crypto" aria-expanded="true" aria-controls="buy-crypto">
                                                                Buy cryptocurrency
                                                                <i class="fa pull-right accordion__angle--animated" aria-hidden="true"></i>
                                                            </h4>
                                                        </div>
                                                        <div id="buy-crypto" class="collapse show" style="">
                                                            <div class="card-body">
                                                                <h6>Select the Crypto Currency
                                                                    <span>‘Minimum Value 0.001 BTC’</span>
                                                                </h6>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="cc BTC"></i>
                                                                        </div>
                                                                    </div>
                                                                    <select class="custom-select">
                                                                        <option value="1">Bitcoin</option>
                                                                        <option value="2">Ethereum</option>
                                                                        <option value="3">Repple</option>
                                                                    </select>
                                                                </div>
                                                                <h6 class="m-t-25">Choose Payment Method</h6>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="icofont icofont-visa"></i>
                                                                        </div>
                                                                    </div>
                                                                    <select class="custom-select">
                                                                        <option value="1">Visa</option>
                                                                        <option value="1">Master</option>
                                                                    </select>
                                                                </div>
                                                                <h6 class="m-t-25">Walet Address</h6>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="icofont icofont-visa"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control" id="" value="Mxs123PWcS596PL5Kid6YZX96">
                                                                </div>
                                                                <h6 class="m-t-25">Exchange Operation</h6>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="icofont icofont-visa"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control" id="" placeholder="Exchange Amount">
                                                                    <div>
                                                                        <i class="fa fa-exchange"></i>
                                                                    </div>
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="icofont icofont-visa"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control" id="" placeholder="Exchange Amount">
                                                                </div>
                                                                <button class="btn btn-success m-t-25">Buy Cryptocurrencies</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 m-b-30">
                                                <div class="accordion">
                                                    <div class="card rounded-0">
                                                        <div class="card-header">
                                                            <h4 class="mb-0" data-toggle="collapse" data-target="#sell-crypto" aria-expanded="true" aria-controls="sell-crypto">
                                                                Sell cryptocurrency
                                                                <i class="fa pull-right accordion__angle--animated" aria-hidden="true"></i>
                                                            </h4>
                                                        </div>
                                                        <div id="sell-crypto" class="collapse show">
                                                            <div class="card-body">
                                                                <h6>Select the Crypto Currency
                                                                    <span>‘Minimum Value 0.001 BTC’</span>
                                                                </h6>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="cc BTC"></i>
                                                                        </div>
                                                                    </div>
                                                                    <select class="custom-select">
                                                                        <option value="1">Bitcoin</option>
                                                                        <option value="2">Ethereum</option>
                                                                        <option value="3">Repple</option>
                                                                    </select>
                                                                </div>
                                                                <h6 class="m-t-25">Crypto Amount</h6>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="icofont icofont-visa"></i>
                                                                        </div>
                                                                    </div>
                                                                    <select class="custom-select">
                                                                        <option value="1">Visa</option>
                                                                        <option value="1">Master</option>
                                                                    </select>
                                                                </div>
                                                                <h6 class="m-t-25">Account Password</h6>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="icofont icofont-visa"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control" id="" placeholder="Enter your password">
                                                                </div>
                                                                <h6 class="m-t-25">Exchange Operation</h6>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="icofont icofont-visa"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control" id="" placeholder="Exchange Amount">
                                                                    <div>
                                                                        <i class="fa fa-exchange"></i>
                                                                    </div>
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="icofont icofont-visa"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control" id="" placeholder="Exchange Amount">
                                                                </div>
                                                                <button class="btn btn-success m-t-25">Place selling order</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 m-b-30">
                                                <div class="accordion">
                                                    <div class="card walet-address-card rounded-0">
                                                        <div class="card-header">
                                                            <h4 class="mb-0" data-toggle="collapse" data-target="#wallet-address" aria-expanded="true" aria-controls="wallet-address">
                                                                Walet Address
                                                                <i class="fa pull-right accordion__angle--animated" aria-hidden="true"></i>
                                                            </h4>
                                                        </div>
                                                        <div id="wallet-address" class="collapse show">
                                                            <div class="card-body">
                                                                <h6>
                                                                    Bitcoin wallet address
                                                                </h6>
                                                                <div class="input-group m-b-30">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="cc BTC" aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control walet-address" id="" value="Mxs123PWid6YZX96" readonly="">
                                                                    <span class="input-group-addon" id="">
                                                                        Copy
                                                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                                                    </span>
                                                                </div>
                                                                <h6 class="m-t-25">Ethereum Walet Address</h6>
                                                                <div class="input-group m-b-30">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="cc ETC" aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control walet-address" id="" value="Mxs123PWid6YZX96" readonly="">
                                                                    <span class="input-group-addon" id="">
                                                                        Copy
                                                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                                                    </span>
                                                                </div>
                                                                <h6 class="m-t-25">Ripple Walet Address</h6>
                                                                <div class="input-group m-b-30">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="cc XRP" aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control walet-address" id="" value="Mxs123PWid6YZX96" readonly="">
                                                                    <span class="input-group-addon" id="">
                                                                        Copy
                                                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                                                    </span>
                                                                </div>
                                                                <h6 class="m-t-25">Litecoin Walet Address</h6>
                                                                <div class="input-group m-b-0">
                                                                    <div class="input-group-prepend">
                                                                        <div class="input-group-text">
                                                                            <i class="cc LTC" aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" class="form-control walet-address" id="" value="Mxs123PWid6YZX96" readonly="">
                                                                    <span class="input-group-addon" id="">
                                                                        Copy
                                                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="walet-direction">
                                                                <h4 class="mb-0">
                                                                    <a href="">Go to Wallet</a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="accordion table-data">
                                                    <div class="card rounded-0">
                                                        <div class="card-header">
                                                            <h4 class="mb-0" data-toggle="collapse" data-target="#table-three" aria-expanded="true" aria-controls="table-three">
                                                                Recent Buying Cryptocurrencies
                                                                <i class="fa pull-right accordion__angle--animated" aria-hidden="true"></i>
                                                            </h4>
                                                        </div>
                                                        <div id="table-three" class="collapse show">
                                                            <table class="table m-b-0">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">ID Number</th>
                                                                        <th scope="col">Trade Time</th>
                                                                        <th scope="col">Status</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>04.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Pending
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>02.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>01.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>04.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Pending
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>05.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>02.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>03.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>05.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Pending
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>04.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="accordion table-data">
                                                    <div class="card rounded-0">
                                                        <div class="card-header">
                                                            <h4 class="mb-0" data-toggle="collapse" data-target="#table-four" aria-expanded="true" aria-controls="table-four">
                                                                Recent Selling Orders
                                                                <i class="fa pull-right accordion__angle--animated" aria-hidden="true"></i>
                                                            </h4>
                                                        </div>
                                                        <div id="table-four" class="collapse show">
                                                            <table class="table m-b-0">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">ID Number</th>
                                                                        <th scope="col">Trade Time</th>
                                                                        <th scope="col">Status</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>04.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Pending
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>02.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>01.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>04.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Pending
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>05.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>02.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>03.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>05.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Pending
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span>ID Number </span>
                                                                            <spna class="id">175896</spna>
                                                                        </td>
                                                                        <td>04.40 am</td>
                                                                        <td>
                                                                            <span class="success">
                                                                                Complete
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                    
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                  
                    
        </div>
    </body>
</html>