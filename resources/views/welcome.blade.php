<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>

@php
    $dataPoints = array();
    $i = 0;
    foreach($crypt_datas as $key=>$values){
        $x = $values['key'];
        array_push($dataPoints, array("x" => date($x) , "y" => $values['value']));
        $i++;
    }
@endphp

<script>
window.onload = function () {
var data_array = <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>;

data_array.forEach(function(obj) { 
    obj.x = new Date(obj.x);

});

var data = [{
        type: "line",
        dataPoints: data_array
    }];
//Better to construct options first and then pass it as a parameter
var options = {
    zoomEnabled: true,
    animationEnabled: true,
    title: {
        text: "History Data"
    },
    axisY: {
        includeZero: false,
        lineThickness: 1
    },
        axisX: {
            labelFormatter: function (e) {
                return CanvasJS.formatDate( e.value, "DD MMM");
            },
        },
    data: data
};

var chart = new CanvasJS.Chart("chartContainer", options);

chart.render();

 
}
</script>
<style>
    #timeToRender {
        position:absolute; 
        top: 10px; 
        font-size: 20px; 
        font-weight: bold; 
        background-color: #d85757;
        padding: 0px 4px;
        color: #ffffff;
    }
</style>

<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
         
    </body>
</html>
