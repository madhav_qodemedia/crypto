<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index')->name('blockwarden');

Route::get('cryptohistory/{symbol}/{currency}', 'FrontendController@displayHistory');

Route::get('historicaldata/{symbol}/{currency}', 'FrontendController@historical');

Route::get('historicalgraph', 'FrontendController@historicalgraph');

Route::get('homepagegraph', 'FrontendController@getGraphData');

Route::get('getcurrencylist', 'HomeController@getCurrencyList');

Route::get('getexchangevalue', 'HomeController@getExchangeValue');

Route::get('getcryptocurrencydata', 'HomeController@getcryptocurrencydata');

Route::get('search', 'HomeController@getcryptocurrencysearch');

Route::get('/purchase', function () {
    return view('purchase');
});
   
